﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WPMSPortal.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WPMS :: Login</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body style="background-image: url(images/energy.jpg); background-size: cover">
    <form id="form1" runat="server">


        <header style="background-color: #010101">
            <div class="container;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3; pull-left">
                            <img src="images/logo.png" />
                        </div>
                        <div class="col-md-9; pull-right;" style="margin-top: 20px">
                            <div style="color: #ffffff;" class="form-group">
                                <label for="txtUsername">Username: </label>
                                <input type="text" id="txtUsername" class="form-control" />
                                <label for="txtPassword">Password: </label>
                                <input type="password" id="txtPassword" class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="container">
            <div class="row">
            </div>

        </div>
    </form>
</body>
</html>
