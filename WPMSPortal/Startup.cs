﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WPMSPortal.Startup))]
namespace WPMSPortal
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
